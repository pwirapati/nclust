/*
 * sparse_downsample.c - subset a sparse matrix, tile, and compute 
 * the mean of each tile 
 *
 * - subset is specified by integer indices, which may contain
 *   replicate selections, or missing values (represented by -1)
 *   and may be arbitrarily re-ordered
 * - averaging is done over integer-sized windows for rows and columns
 * - the last window is padded by missing indices
 * - caller responsible in ensuring rs and cs are within 0:(mx-1)
 *   and 0:(nx-1) 
 */

#include <stdio.h>

#include <stdlib.h>
#include <math.h>

#define NULL_ID -1

void
sparse_downsample (
  int *dims,    // mx, nx, my, ny, mw, nw, ms, ns,
  int *rs,      // [ms] row selected indices
  int *cs,      // [nx] col selected indices
  double *x_x,  // input matrix data (nonzeros)
  int *x_i,     // pointer to row index
  int *x_p,     // [ny+1] index to first nonzero in a column
  double *y,    // [my*ny] output matrix
  double *wrow, // [my] number of present values per row bin
  double *wcol  // [ny] number of present values per col bin
  )
{
  int mx = dims[0];  // input size
  int nx = dims[1];
  int my = dims[2];  // output size
  int ny = dims[3];
  int mw = dims[4];  // window size
  int nw = dims[5];
  int ms = dims[6]; // length of rs
  int ns = dims[7]; // length of cs

  //fprintf(stderr,
  //  "mx = %d, nx = %d, my = %d, ny = %d, mw = %d, nw = %d, ms = %d, ns = %d\n",
  //  mx, nx, my, ny, mw, nw, ms, ns );

  // count the number of of present data in each row and column bins
  for(int u = 0; u < my; u++ )
    {
    wrow[u] = 0;
    for(int uu = 0; uu < mw; uu++ )
      if( rs[u*mw + uu] != NULL_ID )
        wrow[u]++;
    }

  for(int v = 0; v < ny; v++ )
    {
    wcol[v] = 0;
    for(int vv = 0; vv < nw; vv++ )
      if( cs[v*nw + vv] != NULL_ID  )
        wcol[v]++;
    }

  // loop by random accessing the columns of sparse source.
  // For each column, scan all non-zero values of the source

  // make inverse indices for source row index to bin row id
  //
  int *i2u = (int*)malloc(sizeof(int)*mx);
  for(int i = 0; i < mx; i++ )
    i2u[i] = NULL_ID;
  for(int k = 0; k < ms; k++ )
    if( rs[k] != NULL_ID) 
      i2u[rs[k]] = k / mw;
  
  // initialize output
  for(int j = 0; j < ny; j++ )
    {
    if( wcol[j] == 0 )
      {
      for(int i = 0; i < my; i++ )
        y[i + j*my] = NAN;
      continue;
      }
    for(int i = 0; i < my; i++ )
      y[i + j*my] = (wrow[i] == 0 ? NAN : 0 );
    }

  // accumulate the sum
  for(int k = 0; k < ns; k++ )
    {
    int j = cs[k];
    if( j == NULL_ID ) continue;
    int v = k/nw;
    for(int p = x_p[j]; p < x_p[j+1]; p++ )
      {
      int i = x_i[p];
      int u = i2u[i];
      if( u == NULL_ID ) continue;
      y[ u + my*v ] += x_x[p];
      }
    }

  // obtain the mean
  for(int v = 0; v < ny; v++ )
    for(int u = 0; u < my; u++ )
      y[u + v*my] /= wrow[u]*wcol[v];

  free(i2u);
  return;
}
