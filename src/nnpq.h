#ifndef _NNPQ_H_
#define _NNPQ_H_

typedef struct
  {
  int id;
  double S;
  }
nnpq;

extern void
nnpq_heapify( nnpq *q, int n );

extern void
nnpq_push ( nnpq *q, int n, nnpq qin );

#endif // _NNPQ_H_
