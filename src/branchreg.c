#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <Rmath.h>

#include "branchreg.h"

void
branchreg_ttest (
  int *U,
  const int *L,
  const int *R,
  const double *w,
  const double *y, 
  const double *minsz_,     // minimimum branch weight to be considered for split
  const double *minfx_,     // minimum effect size
  const double *maxp_,  // p-value cutoff
  double *Ey
  )
{
  int N = (L[0]+1)/2;

  double *Sw = (double*)malloc(sizeof(double)*2*N*4 );
  double *Swy = Sw + 2*N;
  double *Swyy = Swy + 2*N;
  double *pval = Swyy + 2*N;
  
  for(int i = 0; i < 2*N*4; i++ ) Sw[i] = 0;

  Sw[0] = Swy[0] = Swyy[0] = 0; 
  pval[0] = 1;
  for(int i = 1; i <= N; i++ )
    {
    Sw[i] = w[i-1];
    Swy[i] = y[i-1];
    Swyy[i] = y[i-1]*y[i-1];
    
    pval[i] = 1;
    }
  for(int i = N+1; i <= L[0]; i++ )
    {
    int a = L[i], b = R[i];

    Sw[i] += Sw[a] + Sw[b];
    Swy[i] += Swy[a] + Swy[b];
    Swyy[i] += Swyy[a] + Swyy[b];

    if( Sw[a] < minsz_[0] || Sw[b] < minsz_[0] )
      {
      pval[i] = 1;
      continue;
      }

    double mu_a = Swy[a]/Sw[a];
    double mu_b = Swy[b]/Sw[b];
    double df = Sw[a]+Sw[b]-2;
    double s2 = (Swyy[a] - Sw[a]*mu_a*mu_a + Swyy[b] - Sw[b]*mu_b*mu_b)/df;
    double Tstat = (mu_a - mu_b)*sqrt( Sw[a]*Sw[b]/(Sw[a]+Sw[b])/s2);
    pval[i] = 2*pt( fabs(Tstat), df, 0,0 );
    if( (pval[i] < maxp_[0] && fabs(mu_a-mu_b) > minfx_[0]) 
        || U[L[i]]== -1 || U[R[i]]== -1
        )
      U[i] = -1; // flag subtree to be cut
    }

  if(U[L[0]] == 0 ) // no significant branch, the root is the only cut subtree
    U[L[0]] = L[0];
  
  // propagate link to the roots of cut subtrees
  for(int i = L[0]; i > N; i-- )
    {
    if( U[i] == -1 )
      {
      if( U[L[i]] != -1 ) U[L[i]] = L[i];
      if( U[R[i]] != -1 ) U[R[i]] = R[i];
      }
    else
      U[L[i]] = U[R[i]] = U[i];
    }

  for(int i = 1; i <= N; i++ )
    Ey[i-1] = Swy[ U[i]]/Sw[U[i]];

  free(Sw);
}

void
branchreg_x2test (
  int *U,
  const int *L,
  const int *R,
  const double *w,
  const double *y, 
  const double *minsz_,     // minimimum branch weight to be considered for split
  const double *minfx_,     // minimum effect size
  const double *maxp_,  // p-value cutoff
  double *Ey
  )
{
  int N = (L[0]+1)/2;

  double *Sw = (double*)malloc(sizeof(double)*2*N*3 );
  double *Swy = Sw + 2*N;
  double *pval = Swy + 2*N;
  
  for(int i = 0; i < 2*N*4; i++ ) Sw[i] = 0;

  Sw[0] = Swy[0] = 0; 
  pval[0] = 1;
  for(int i = 1; i <= N; i++ )
    {
    Sw[i] = w[i-1];
    Swy[i] = y[i-1];
    pval[i] = 1;
    }

  for(int i = N+1; i <= L[0]; i++ )
    {
    int a = L[i], b = R[i];

    Sw[i] += Sw[a] + Sw[b];
    Swy[i] += Swy[a] + Swy[b];

    if( Sw[a] < minsz_[0] || Sw[b] < minsz_[0] )
      {
      pval[i] = 1;
      continue;
      }
    double Ta = Sw[a]-Swy[a], Tb = Sw[b] - Swy[b];

    double fx = log( Swy[a]/Ta / Swy[b] * Tb);
  
    double d = fabs(Swy[a]*Tb - Swy[b]*Ta) - Sw[i]/2;
    d = (d < 0 ? 0 : d*d );
    double chisq_yates = Sw[i]*d/(Sw[a]*Sw[b]*(Swy[a]+Swy[b])*(Ta+Tb));
    pval[i] = pchisq( chisq_yates,1,0,0 );
    if( (pval[i] < maxp_[0] && fabs(fx) > minfx_[0]) 
        || U[L[i]]== -1 || U[R[i]]== -1
        )
      U[i] = -1; // flag subtree to be cut
    }

  if(U[L[0]] == 0 ) // no significant branch, the root is the only cut subtree
    U[L[0]] = L[0];
  
  // propagate link to the roots of cut subtrees
  for(int i = L[0]; i > N; i-- )
    {
    if( U[i] == -1 )
      {
      if( U[L[i]] != -1 ) U[L[i]] = L[i];
      if( U[R[i]] != -1 ) U[R[i]] = R[i];
      }
    else
      U[L[i]] = U[R[i]] = U[i];
    }

  for(int i = 1; i <= N; i++ )
    Ey[i-1] = Swy[ U[i]]/Sw[U[i]];

  free(Sw);
}

