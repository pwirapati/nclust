#ifndef _BRANCHREG_H_
#define _BRNACHREG_H_

void
branchreg_ttest (
  int *U,
  const int *L,
  const int *R,
  const double *w,
  const double *y, 
  const double *minsz_,    // minimimum branch size to be considered for split
  const double *minfx_,    // minimum effect size
  const double *maxp_,  // p-value cutoff
  double *Ey
  );

void
branchreg_x2test (
  int *U,
  const int *L,
  const int *R,
  const double *w,
  const double *y, 
  const double *minsz_,     // minimimum branch weight to be considered for split
  const double *minfx_,     // minimum effect size
  const double *maxp_,  // p-value cutoff
  double *Ey
  );
#endif // _BRANCHREG_H_
