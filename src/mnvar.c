/*
 * mnvar.c
 *
 */
#include <stdlib.h>
#include <math.h>
#include <stdio.h>

#include "nnc.h"
#include "nclustree.h"

/*
We use the negative of the variance or SSQ (due to nnc works
by maximizing the score).


Weighting scheme:

1. marginal feature weights are equal (assumed to be 1), item weights
   are either all ones or given (set as initial values)
2. arbitrary feature weights (a vector)
3. arbitrary feature-item matrix: uses 3 x M x 2N storage (w_ij set
   for leaf nodes as mm^0)

The original y_ij was never directly needed. They are used only
via mm^1 and mm^2.

MNSSQ uses very similar calculation and can be implemented together.

*/

typedef struct
  {
  int M;      // number of features
  int N;      // number of leaf nodes
  int method; // 0 MNSSQ, 1 MNVAR
  int mode;   // 0 marginal item weights, 1= marginal feature weights, 2 = cell weights
  double *mm; // [2*M*2N] or [3*M*2N] moments
  double *t;  // [M] marginal row weights (can be NULL if mode = 0)
  double *u;  // [2N] column weights 
  double St;  // sums or row weights (equals to M if all equals to one)
  }
mnvar_data;

void
mnvar_scan(
  void *data,
  int j,
  int nc,
  int *c_,
  double *Sj_
  )
{
  mnvar_data *D=(mnvar_data*)data;
  int M = D->M;
  double *mm = D->mm;
  double *u = D->u;
  if( D->method ) // MNVAR
    {
    if( D->mode == 0)
      {
      double *mm_j = mm + 2*M*j;
      
      #pragma omp parallel for schedule(runtime)
      for(int k = 0; k < nc; k++ )
        {
        double *mm_k = mm + 2*M*c_[k];
        Sj_[k] = 0;
        double S0 = u[j] + u[c_[k]];

        #pragma omp simd
        for(int i = 0; i < M; i++ )
          {
          int i2 = 2*i;
          double S1 = mm_j[i2] + mm_k[i2];
          double S2 = mm_j[i2+1] + mm_k[i2+1];
          double mu = S1/S0;
          Sj_[k] -= S2/S0 - mu*mu;
          }
        Sj_[k] /= M;
        }
      }
    else if( D->mode == 1)
      {
      double *mm_j = mm + 2*M*j;
      
      #pragma omp parallel for schedule(runtime)
      for(int k = 0; k < nc; k++ )
        {
        double *mm_k = mm + 2*M*c_[k];
        Sj_[k] = 0;
        double S0 = u[j] + u[c_[k]];

        #pragma omp simd
        for(int i = 0; i < M; i++ )
          {
          int i2 = 2*i;
          double S1 = mm_j[i2] + mm_k[i2];
          double S2 = mm_j[i2+1] + mm_k[i2+1];
          double mu = S1/S0;
          Sj_[k] -= D->t[i] * (S2/S0 - mu*mu);
          }
        Sj_[k] /= D->St;
        }
      }
    else
      {
      double *mm_j = mm + 3*M*j;
      
      #pragma omp parallel for schedule(runtime)
      for(int k = 0; k < nc; k++ )
        {
        double *mm_k = mm + 3*M*c_[k];
        Sj_[k] = 0;

        #pragma omp simd
        for(int i = 0; i < M; i++ )
          {
          int i3 = 3*i;
          double S0 = mm_j[i3] + mm_k[i3];
          double S1 = mm_j[i3+1] + mm_k[i3+1];
          double S2 = mm_j[i3+2] + mm_k[i3+2];
          double mu = S1/S0;
          Sj_[k] -= S2/S0 - mu*mu;
          }
        Sj_[k] /= M;
        }
      }
    }
  else // MNSSQ
    {
    if( D->mode == 0)
      {
      double *mm_j = mm + 2*M*j;
      
      #pragma omp parallel for schedule(runtime)
      for(int k = 0; k < nc; k++ )
        {
        double *mm_k = mm + 2*M*c_[k];
        Sj_[k] = 0;
        double S0 = u[j] + u[c_[k]];

        #pragma omp simd
        for(int i = 0; i < M; i++ )
          {
          int i2 = 2*i;
          double S1 = mm_j[i2] + mm_k[i2];
          double S2 = mm_j[i2+1] + mm_k[i2+1];
          Sj_[k] -= S2 - S1*S1/S0;
          }
        Sj_[k] /= M;
        }
      }
    else if( D->mode == 1)
      {
      double *mm_j = mm + 2*M*j;
      
      #pragma omp parallel for schedule(runtime)
      for(int k = 0; k < nc; k++ )
        {
        double *mm_k = mm + 2*M*c_[k];
        Sj_[k] = 0;
        double S0 = u[j] + u[c_[k]];

        #pragma omp simd
        for(int i = 0; i < M; i++ )
          {
          int i2 = 2*i;
          double S1 = mm_j[i2] + mm_k[i2];
          double S2 = mm_j[i2+1] + mm_k[i2+1];
          Sj_[k] -= D->t[i] * (S2 - S1*S1/S0);
          }
        Sj_[k] /= D->St;
        }
      }
    else
      {
      double *mm_j = mm + 3*M*j;
      
      #pragma omp parallel for schedule(runtime)
      for(int k = 0; k < nc; k++ )
        {
        double *mm_k = mm + 3*M*c_[k];
        Sj_[k] = 0;

        #pragma omp simd
        for(int i = 0; i < M; i++ )
          {
          int i3 = 3*i;
          double S0 = mm_j[i3] + mm_k[i3];
          double S1 = mm_j[i3+1] + mm_k[i3+1];
          double S2 = mm_j[i3+2] + mm_k[i3+2];
          Sj_[k] -= S2 - S1*S1/S0;
          }
        Sj_[k] /= M;
        }
      }
    }
}

void mnvar_merge(
  void *data,
  int c,
  int a,
  int b
  )
{
  mnvar_data *D=(mnvar_data*)data;
  double *u = D->u;
  double *mm = D->mm;
  int M = D->M;
  if( D->mode < 2 )
    {
    double *mm_a = mm + 2*M*a; 
    double *mm_b = mm + 2*M*b; 
    double *mm_c = mm + 2*M*c;

    u[c] = u[a] + u[b];
    #pragma omp simd
    for(int i = 0; i < M; i++ )
      {
      int i2 = i*2;
      mm_c[i2] = mm_a[i2] + mm_b[i2];
      mm_c[i2+1] = mm_a[i2+1] + mm_b[i2+1];
      }
    }
  else // weighted cells
    {
    double *mm_a = mm + 3*M*a; 
    double *mm_b = mm + 3*M*b; 
    double *mm_c = mm + 3*M*c;

    #pragma omp simd
    for(int i = 0; i < M; i++ )
      {
      int i3 = 3*i;
      mm_c[i3] = mm_a[i3] + mm_b[i3];
      mm_c[i3+1] = mm_a[i3+1] + mm_b[i3+1];
      mm_c[i3+2] = mm_a[i3+2] + mm_b[i3+2];
      }
    } 
}

double
mnvar_score (
  void *data,
  int a,
  int b
  )
{
  mnvar_data *D=(mnvar_data*)data;
  int M = D->M;
  double *mm = D->mm;
  double score = 0;
  if( D->method ) // MNVAR
    {
    if( D->mode == 0 )
      {
      double *mm_a = mm + 2*M*a;
      double *mm_b = mm + 2*M*b;
      double S0 = D->u[a] + D->u[b];
      for(int i = 0; i < M; i++ )
        {
        int i2 = i*2;
        double S1 = mm_a[i2] + mm_b[i2];
        double S2 = mm_a[i2+1] + mm_b[i2+1];
        double mu = S1/S0;
        score -= S2/S0 - mu*mu;
        }
      score /= M;
      }
    else if( D->mode == 1 )
      {
      double *mm_a = mm + 2*M*a;
      double *mm_b = mm + 2*M*b;
      double S0 = D->u[a] + D->u[b];
      for(int i = 0; i < M; i++ )
        {
        int i2 = i*2;
        double S1 = mm_a[i2] + mm_b[i2];
        double S2 = mm_a[i2+1] + mm_b[i2+1];
        double mu = S1/S0;
        score -= D->t[i] * (S2/S0 - mu*mu);
        }
      score /= D->St;
      }
    else // mode == 2
      {
      double *mm_a = mm + 3*M*a;
      double *mm_b = mm + 3*M*b;
      for(int i = 0; i < M; i++ )
        {
        int i3 = i*3;
        double S0 = mm_a[i3] + mm_b[i3];
        double S1 = mm_a[i3+1] + mm_b[i3+1];
        double S2 = mm_a[i3+2] + mm_b[i3+2];
        double mu = S1/S0;
        score -= S2/S0 - mu*mu;
        }
      score /= M;
      }
    }
  else // MNSSQ
    {
    if( D->mode == 0 )
      {
      double *mm_a = mm + 2*M*a;
      double *mm_b = mm + 2*M*b;
      double S0 = D->u[a] + D->u[b];
      for(int i = 0; i < M; i++ )
        {
        int i2 = i*2;
        double S1 = mm_a[i2] + mm_b[i2];
        double S2 = mm_a[i2+1] + mm_b[i2+1];
        score -= S2 - S1*S1/S0;
        }
      score /= M;
      }
    else if( D->mode == 1 )
      {
      double *mm_a = mm + 2*M*a;
      double *mm_b = mm + 2*M*b;
      double S0 = D->u[a] + D->u[b];
      for(int i = 0; i < M; i++ )
        {
        int i2 = i*2;
        double S1 = mm_a[i2] + mm_b[i2];
        double S2 = mm_a[i2+1] + mm_b[i2+1];
        score -= D->t[i] * (S2 - S1*S1/S0);
        }
      score /= D->St;
      }
    else // mode == 2
      {
      double *mm_a = mm + 3*M*a;
      double *mm_b = mm + 3*M*b;
      for(int i = 0; i < M; i++ )
        {
        int i3 = i*3;
        double S0 = mm_a[i3] + mm_b[i3];
        double S1 = mm_a[i3+1] + mm_b[i3+1];
        double S2 = mm_a[i3+2] + mm_b[i3+2];
        score -= S2 - S1*S1/S0;
        }
      score /= M;
      }
    }

  return score;
}

void
nclust_mnvar(
  const int *dims,  // M, N
  const int *options, 

  const double *y, // [M*N] data matrix
  const double *w, // optional [M*N] weight matrix (if mode == 2)

  double *t,    // feature weights [optional]
  double *u,

  int *L, int *R, int *U,
  double *S,
  int *order,
  int *nleaf,
  int *leftmost,
  int *level,
  int *retstat
  )
{
  int M = dims[0], N = dims[1];
  
  mnvar_data data;
  data.M = M;
  data.N = N;
  data.t = t;
  data.u = u;
  const int cache_length = options[0];
  const int branchflip = options[1];
  // options[2] standardize is not used here
  const int verbose = options[3];

  // MNVAR specific
  data.method = options[4];
  data.mode = options[5];

  if( data.mode == 1 )
    {
    data.St = 0;
    for(int i = 0; i < M; i++ )
      data.St += t[i];
    }
  else
    data.St = M;

  // initialized moments
  data.mm = (double*)malloc(sizeof(double) * (data.mode==2 ? 3: 2) * M * 2*N );
  if( data.mode < 2 )
    {
    for(int j = 1; j <= N; j++ )
      {
      double *mm_j = data.mm + 2*M*j;
      const double *y_j = y + M*(j-1);
      for(int i = 0; i < M; i++ )
        {
        int i2 = i*2;
        mm_j[i2] = y_j[i]*u[j];
        mm_j[i2+1] = mm_j[i2] * y_j[i];
        }
      }
    for(int j = N+1; j < 2*N; j++ )
      {
      double *mm_j = data.mm + 2*M*j;
      for(int i = 0; i < 2*M; i++ )
        mm_j[i] = 0;
      }
    }
  else // mode == 2, weights for each data element
    {
    for(int j = 1; j <= N; j++ )
      {
      double *mm_j = data.mm + 3*M*j;
      const double *y_j = y + M*(j-1);
      const double *w_j = w + M*(j-1);
      for(int i = 0; i < M; i++ )
        {
        int i3 = i*3;
        mm_j[i3] = w_j[i];
        mm_j[i3+1] = mm_j[i3] * y_j[i];
        mm_j[i3+2] = mm_j[i3+1] * y_j[i];
        }
      }
    for(int j = N+1; j < 2*N; j++ )
      {
      double *mm_j = data.mm + 3*M*j;
      for(int i = 0; i < 3*M; i++ )
        mm_j[i] = 0;
      }
    }

  nnc( N, &data, mnvar_scan, mnvar_merge, L, R, U, S, cache_length, verbose );

  if( branchflip == 1 )
    {
    if( S[ L[R[0]] ] < S[ R[R[0]] ] )
      SWAP(int, L[R[0]], R[R[0]] );
    branchflip_nnephew( U, L, R, &data, mnvar_score );
    }
  else if(branchflip == 2)
    branchflip_tightleft( N, U, L, R, S );
  else if(branchflip == 3)
    branchflip_tightright( N, U, L, R, S );

  branch_nleaf( U, L, R, nleaf );
  leafordering( U, L, R, order, leftmost );
  branch_level( &N, U, level );

  *retstat = 0;
  free(data.mm);
  return;
}
