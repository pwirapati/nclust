#include "nnpq.h"

void
nnpq_heapify( nnpq *q, int n )
{
  for(int i = 1; i < n; i++ )
    {
    nnpq tmp;
    if( q[i].S > q[0].S )
      {
      tmp = q[i];
      q[i] = q[0];
      q[0] = tmp;
      }
    
    tmp = q[i];
    int j = i;
    for(int k = j/2; k > 0 && (q[k].S > tmp.S); k /= 2 )
      {
      q[j] = q[k];
      j = k;
      }
    q[j] = tmp;
    }

  return;
}

void
nnpq_push ( nnpq *q, int n, nnpq qin )
{
  if( n == 1 )
    {
    if( qin.S > q[0].S )
      q[0] = qin;
    return;
    }

  if( qin.S < q[1].S ) // ignore if smaller than the smallest
    return;  

  if( qin.S > q[0].S ) // swap if better than maximum
    {
    q[1] = q[0];
    q[0] = qin;
    qin = q[1];
    }

  int j = 1;
  for(int k = 2; k < n; k *= 2 )
    {
    if( (k+1 < n) && (q[k].S > q[k+1].S) )
      k++;

    if( qin.S <= q[k].S )
      break;

    q[j] = q[k];
    j = k;
    }
  q[j] = qin;

  return;
}

