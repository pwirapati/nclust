#ifndef _BIFF_H_
#define _BIFF_H_

extern void
biff (
  const int *dims,
  const int *U,
  const int *L,
  const int *R,
  const double *w,
  const double *y,
  const int *ind,
  const int *ptr,
  const double *par,
  const double *min_w_,
  int *node_id,
  double *node_score,
  int *node_id2,
  double *node_score2
  );

#endif 
