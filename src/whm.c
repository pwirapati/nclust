/*
 * whm.c - weighted heatmap
 *
 */

#include "etc.h"

void
wxform(
  int *nv_, int *nw_,  // length of source and target
  double *v,           // [nv] weight for the source

  int *pa,             // [nv+1] source index pointer
  int *ixa,            // [nv+nw-1] indices of target
  double *a            // [nv+nw-1] coeffs for target
  )
{
  int nv = *nv_, nw = *nw_;
  
  // cumulative sum
  for(int i = 1; i < nv; i++ )
    v[i] += v[i-1];

  double delta = v[nv-1] / nw;

  int i = 0, j = 0, k = 0; 
  pa[i] = 0;
  double u = 0;
  while( i < nv )
    {
    ixa[k] = j;
    double wj = j < nw-1 ? (j+1)*delta : v[nv-1];
    if( v[i] <= wj )
      {
      a[k] = (v[i]-u)/delta;
      u = v[i];
      if( v[i] == wj )
        { j++; wj += delta; }
      i++;
      k++;
      pa[i] = k;
      }
    else
      {
      a[k] = (wj - u)/delta;
      u = wj;
      j++;
      wj += delta;
      k++;
      }
    }
  pa[i] = k;
}

// render weighted sparse matrix into a dense regular matrix
void
whm_render (
  const int *dims,  // [4] my,ny, mz, nz
  double *rw, // [my] row weights, replaced by cumulative weights
  double *cw, // [ny] column weights, replaced by cumulative weights
  
  // sparse matrix to render
  const int *py,  // [ny+1]
  const int *ixy, // [ py[ny] ]
  const double *y,// [ py[ny] ]

  // dense matrix output
  double *z      // [mz * nz] assumed initialized (rendering add on top)
  )
{
  int my = dims[0], ny = dims[1], mz = dims[2], nz = dims[3];
  int *tmpi = (int*) nalloc( sizeof(int),
                          my+1 + ny+1 + (my+mz-1) + (ny+nz-1));
  int *rpa = tmpi;
  int *cpa = rpa + (my+1);
  int *rixa = cpa + (ny+1);
  int *cixa = rixa + (my+mz-1);

  double *tmpd = (double*)nalloc(sizeof(double), (my+mz-1) + (ny+nz-1) );
  double *ra = tmpd;
  double *ca = ra + (my+mz-1);

  wxform( &my, &mz, rw, rpa, rixa, ra );

  wxform( &ny, &nz, cw, cpa, cixa, ca );

  if(py[0] >= 0 ) // sparse matrix
    {
    for(int jy = 0; jy < ny; jy++ )
      for(int l = cpa[jy]; l < cpa[jy+1]; l++ )
        {
        int jz = cixa[l];
        for(int h = py[jy]; h < py[jy+1]; h++ )
          {
          int iy = ixy[h];
          double zij = ca[l]*y[h];
          for(int k = rpa[iy]; k < rpa[iy+1]; k++ )
            {
            int iz = rixa[k];
            z[ iz + mz * jz ] += ra[k]*zij;
            }
          }
        }
    }
  else // dense matrix
    {
    for(int jy = 0; jy < ny; jy++ )
      for(int l = cpa[jy]; l < cpa[jy+1]; l++ )
        {
        int jz = cixa[l];
        for(int iy = 0; iy < my; iy++ )
          {
          double zij = ca[l]*y[iy + my*jy];
          for(int k = rpa[iy]; k < rpa[iy+1]; k++ )
            {
            int iz = rixa[k];
            z[ iz + mz * jz ] += ra[k]*zij;
            }
          }
        }
    }
  nfree(tmpi);
  nfree(tmpd);
}
