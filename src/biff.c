#include <stdlib.h>
#include <math.h>
#include <Rmath.h>

#include "biff.h"

#include <stdio.h>

void
biff (
  const int *dims,
  const int *U,
  const int *L,
  const int *R,
  const double *w,

  const double *y,
  const int *ind,
  const int *ptr,

  const double *par,
  const double *min_w_,

  int *node_id,
  double *node_score,
  int *node_id2,
  double *node_score2
  )
{
  const int N = dims[0], M = dims[1]; // N: # items, M: # vars
  const int is_sparse = dims[2];
  const double kappa = par[0], lambda = par[1];

  double *Sw = (double*)malloc(sizeof(double)*2*N * 2 );
  double *Swy = Sw + 2*N;

  double Sw_total = 0;
  for(int i = 0; i < N; i++ )
    Sw_total += w[i];

  for(int j = 0; j < M; j++ )
    {
    // set the leaf nodes values
    //
    Sw[0] = Swy[0] = 0;
    for(int i = 1; i <= N; i++ )  // can be moved out of the loop
      Sw[i] = w[i-1];

    if( is_sparse )
      {
      for(int i = 1; i <= N; i++ )
        Swy[i] = 0;
      for(int k = ptr[j]; k < ptr[j+1]; k++ )
        Swy[ ind[k]+1 ] = y[k];
      }
    else
      {
      const double *yj = y + N*j;
      for(int i = 1; i <= N; i++ )
        Swy[i] = yj[i-1];
      }

    // accumulate towards the root
    //

    double max_score = 0;
    int max_node = 0;
    double max_score2 = 0;
    int max_node2 = 0;
    for(int i = N+1; i <= L[0]; i++ )
      {
      int a = L[i], b = R[i];

      Sw[i] = Sw[a] + Sw[b];
      Swy[i] = Swy[a] + Swy[b];

      if(Sw[i] < *min_w_ ) continue;

      double mu_a = Swy[a]/Sw[a];
      double mu_b = Swy[b]/Sw[b];
      double d = mu_b - mu_a; 
      double pa = Sw[a]/Sw[i];
      double pb = Sw[b]/Sw[i];
      double q = Sw[i]/Sw_total;

      double score = d;

      if(isfinite(kappa))
        score *= sqrt(q/(q+kappa)*(1+kappa));
      else
        score *= sqrt(q);

      if(isfinite(lambda))
        score *= (1+2*lambda)*sqrt( pa/(pa+lambda)*pb/(pb+lambda));
      else
        score *= sqrt(pa*pb);
     
      if( fabs(score) > fabs(max_score) )
        {
        max_score2 = max_score;
        max_score = score;
        max_node2 = max_node;
        max_node = i;
        }
      else if( fabs(score) > fabs(max_score2) )
        {
        max_score2 = score;
        max_node2 = i;
        }
      }
      node_id[j] = max_score < 0 ? L[max_node] : R[max_node];
      node_score[j] = fabs(max_score);
      node_id2[j] = max_score2 < 0 ? L[max_node2] : R[max_node2];
      node_score2[j] = fabs(max_score2);
    }

  free(Sw);
}
