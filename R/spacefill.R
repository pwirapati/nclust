spacefill <- function(
  clust,
  root = clust$R[1],
  bbox = c(-1,1,-1,1),
  horiz=TRUE
  )
{
  if(length(bbox)!=4) stop("length(bbox) != 4")
  r <- .C("spacefill",
    root=as.integer(root),
    U=as.integer(clust$U),
    L=as.integer(clust$L),
    R=as.integer(clust$R),
    w=as.double(clust$wnode),
    bbox=as.double(bbox),
    horiz=as.integer(horiz),
    x=double(clust$N),
    y=double(clust$N),
    PACKAGE="nclust"
    )
  list(x=r$x,y=r$y)
} 
