nclust <- function(
  data,
  w = NULL,
  wfeat = NULL, # feature weights
  witem = NULL, # item weights
  derr = NA, # data error variance (NA: none, NULL: auto, vector: given values)
  method = "avedot",
  branchflip = "center",
  standardize = NULL,
  autofix.inversion = FALSE,
  cache_length = 32,
  verbose = 1
  )
{
  if( method == "MNVAR" || method == "MNSSQ" )
    {
    if(!is.numeric(data) || !is.matrix(data))
      stop("data has to be numeric matrix for MNSSQ or MNVAR")
    return( nclust_mnvar(data,w=w,wfeat=wfeat,witem=witem,
        method=method,branchflip=branchflip,
        autofix.inversion=autofix.inversion,cache_length=cache_length,
        verbose=verbose) )
    }

  x <- NULL
  if( class(data)[1] == "tile" )
    {
    x <- data$x
    if(!is.null(data$w)) w <- data$w
    }
  else if(is.matrix(data)) 
    x <- data

  if( length(dim(x)) != 2 ) stop("length(dim(x)) != 2")
  if( !is.null(w) && any(dim(w) != dim(x)) ) stop("dim(w) != dim(x)")
  M <- nrow(x)
  N <- ncol(x)
  if( cache_length < 1 ) cache_length <- 1

  xx <- cbind( rep(0,M), x, array( 0, dim=c(M,N-1) ))
  if( is.null(w) )
    ww <- NULL
  else
    ww <- cbind( rep(0,M), w, array( 0, dim=c(M,N-1) ))
  
  if(is.null(wfeat))
    wfeat <- rep(1,M)
  else
    {
    if(length(wfeat) != M) stop("length(wfeat) != M")
    wfeat <- ifelse(wfeat < 0, 0, wfeat)
    }

  if(is.null(witem))
    wnode <- c(0,rep(1,N),rep(0,N-1))
  else
    {
    if(length(witem) != N ) stop("length(witem) != N")
    wnode <- c(0, ifelse(witem < 0, 0, witem), rep(0,N-1))
    }

  if(is.na(method_id <- pmatch(method,c("avedot","ward"))))
    stop("invalid `method`")

  if(is.na(branchflip <- pmatch(branchflip,c("center","left","right"))))
    stop("invalid `branchflip`")

  if(is.null(standardize))
    {
    if( method == "avedot" ) standardize <- TRUE
    else standardize <- FALSE
    }

  standardize <- as.logical(standardize)
  if(is.na(standardize)) standardize <- FALSE

  with_derr <- 1
  if(is.null(derr)) # auto
    {
    derr <- c(0,
      colSums(1/xx * wfeat * wfeat),
      rep(0,N-1))
    }
  else if(is.vector(derr) && is.na(derr[1]))
    {
    derr <- c()
    with_derr <- 0
    }
  else if(is.vector(derr) && length(derr)==N)
    {
    derr <- c(0,derr,rep(0,N-1))
    }
  else
    stop("derr has wrong format")

  r <- .C("wf_dense_nclust",
    dims = as.integer( c(ifelse(is.null(w),1,2),M,N) ),
    options = as.integer( 
      c(cache_length, branchflip, standardize, verbose, method_id, with_derr)),

    ## input
    xx = as.double(xx),
    ww = as.double(ww), # ?
    wfeat = as.double(wfeat),
    wnode = as.double(wnode),
    derr = as.double(derr),

    ## tree output
    L = integer(2*N), R = integer(2*N), U = integer(2*N),
    S = double(2*N),
    order = integer(N),
    nleaf = integer(2*N),
    leftmost = integer(2*N),
    level = integer(2*N),

    wleftmost = double(2*N),

    retstat = integer(1), # return status
    NAOK=FALSE,
    PACKAGE="nclust"
    ) 
  if(r$retstat != 0 ) stop("nnc failed")
  r$retstat <- NULL

  r$dims <- r$dims[3]
  names(r)[1] <- "N"
  r$options <- NULL
  r$xx <- NULL
  r$ww <- NULL
  if( !is.null(colnames(x)) ) r$labels <- colnames(x)
  r$method <- method
  if(method == "ward")
    r$S <- -sqrt(abs(r$S))
  r$branchflip <- c("center","left","right")[branchflip]

  rr <- check.inversion(r,verbose=verbose)
  if(autofix.inversion && rr$n.inversion > 0) r <- rr
  class(r) <- "nclust"
  invisible(r)
}
