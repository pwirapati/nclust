biffls <- function(bf,min_nleaf=4,min_gset=25,max_gset=Inf,
  n_shown=5,n_cols=4)
{
  bf <- bf[ bf$nleaf >= min_nleaf,]
  g <- by( rownames(bf), as.factor(bf$node_id), list )
  len_gset <- sapply(g,length)
  g <- g[ len_gset >= min_gset & len_gset <= max_gset ]
  g <- lapply(g,function(u) u[1:min(n_shown,length(u))])
  N <- length(g)
  lapply( 1:n_cols,function(i)
    g[ seq(i,N,n_cols) ] )
}
